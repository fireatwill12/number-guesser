import React from 'react';
import { Alert, StyleSheet, Button, Text, TextInput, View, Keyboard, TouchableWithoutFeedback} from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.maxRandomNumber = 5;
    
    // Set state
    this.state = {
      userInput: '',
      number: parseInt(Math.floor(Math.random() * this.maxRandomNumber) + 1),
      responseText: '',
      gameState: false
    }

    console.log(this.state.number);
  }

  // I can't believe this is the syntax to bind functions to this.
  // See #5 at https://medium.freecodecamp.org/react-binding-patterns-5-approaches-for-handling-this-92c651b5af56
  // for more information.
  determineIfGameIsOver = () => {  
    console.log("User input:", this.state.userInput);

    if(this.state.userInput !== ''){
      Keyboard.dismiss();
    }

    if(this.state.number === parseInt(this.state.userInput)){
      this.setState({
        responseText: "\nYou guessed it! Good job! 🎉🎊🎈\n",
        gameState: true
      });
    } else {
      this.setState({responseText: "\nNope, that's not it! Try again :)\n"});
    }
  }

 /* Function resetGame
  * 
  * Resets the game state. Specifically, this:
  *   - Sets that gameState boolean to false
  *   - Chooses a new random number
  * 
  * Called after a game has been won and the user pushes the "New Game" button.
  */
  resetGame = () => {
    this.setState({
      number: parseInt(Math.floor(Math.random() * this.maxRandomNumber) + 1),
      responseText: '',
      userInput: '',
      gameState: false
    });

    this.textInput.clear();
  }

  render() {
    return (
      // Main screen background
        <View style={styles.container}>
        
          {/* Prompt Text */}
          <Text style={styles.text}>
            I'm thinking of a number between 1 and {this.maxRandomNumber}...care to guess?{"\n"}
          </Text>

          {/* Text Input Field */}
          <View style={{padding: 10}}>
            <Text style={styles.text}>Guess here:</Text>

            <TextInput 
              ref={input => { this.textInput = input }}
              style={{height: 40}}
              placeholder="Your Guess"
              onChangeText={(userInput) => {
                  this.setState({userInput}); 
                }
              }
                
              keyboardType="numeric"
            />

            <Button
              title="Guess"
              onPress={this.determineIfGameIsOver}
              disabled={this.state.gameState}
            />
          </View>


          <Text style={styles.text}>
            {this.state.responseText}
          </Text>

          <Button
            onPress={this.resetGame}
            title="New Game"
          />
          
        </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'deepskyblue',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'whitesmoke',
    fontSize: 20,
    textAlign: 'center'
  },
  textInput: {
    textAlign: 'center',
    fontSize: 20,
    color: 'whitesmoke'
  }
});
