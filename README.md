# Number Guesser

A crude number guessing game, built in React Native. My Hello World for React Native!

### Things Learned

* You can use the $function_name = () => { syntax to bind a function to *this*
* *this.variableName* and *this.state.variableName* are two very different things! The React Component's state and the class's instance variables are two separate scopes.
* JavaScript's ? syntax is an easy and fun way to shorten what would otherwise be a trite if/else conditional to set a variable's value.